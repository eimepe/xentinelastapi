module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),

  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '806768a9f8b41409c6c9168ece2c37cb'),
    },
  },
});
